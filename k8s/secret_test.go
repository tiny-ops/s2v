package k8s

import (
	"log"
	"os"
	"testing"
)

func TestGetSecretsFromManifest(t *testing.T) {
	logger := log.New(os.Stderr, "", log.LstdFlags)

	fileContent, err := os.ReadFile("../test-data/secret.yaml")
	if err != nil {
		t.Fatalf("config read error: %s\n", err.Error())
	}

	manifest := string(fileContent)

	opts := SecretDecodingOptions{
		StopOnDecodeError: false,
	}

	secrets, err := GetSecretsFromManifest(&manifest, &opts, logger)

	if err != nil {
		t.Fatalf("unexpected error: %s\n", err.Error())
	}

	databaseUrl, exists := secrets["DATABASE_URL"]

	if !exists {
		t.Fatal("DATABASE_URL key not found")
	}

	if databaseUrl != "app.db" {
		t.Fatalf("Unexpected DATABASE_URL value, expected 'app.db', got '%s'", databaseUrl)
	}

	databaseUser, exists := secrets["DATABASE_USER"]

	if !exists {
		t.Fatal("DATABASE_USER key not found")
	}

	if databaseUser != "demo-app-user" {
		t.Fatalf("Unexpected DATABASE_USER value, expected 'demo-app-user', got '%s'", databaseUser)
	}

	databasePassword, exists := secrets["DATABASE_PASSWORD"]

	if !exists {
		t.Fatal("DATABASE_PASSWORD key not found")
	}

	if databasePassword != "1029j09qelDAm" {
		t.Fatalf("Unexpected DATABASE_PASSWORD value, expected '1029j09qelDAm', got '%s'", databasePassword)
	}

	token, exists := secrets["TOKEN"]

	if !exists {
		t.Fatal("TOKEN key not found")
	}

	if token != "non-encoded-value" {
		t.Fatalf("Unexpected TOKEN value, expected 'non-encoded-value', got '%s'", token)
	}
}
