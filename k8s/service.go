package k8s

import (
	"fmt"
	"strings"
)

func ExtractServiceNameFromSecretName(secretName string, secretSuffixes []string) string {
	result := secretName

	for i := range secretSuffixes {
		suffix := secretSuffixes[i]
		result = strings.TrimSuffix(result, fmt.Sprintf("-%s", suffix))
	}

	result = strings.TrimRight(result, "-")

	return result
}
