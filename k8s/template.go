package k8s

import (
	"bytes"
	"fmt"
	"log"
	"sort"
	"strings"
	"text/template"
)

func GenerateSecretManifestFromTemplate(templateFileName string, namespace string, secretName string,
	serviceName string, secretsBlock string, logger *log.Logger) (string, error) {
	logger.Printf("generating secret manifest from template '%s'..\n", templateFileName)
	logger.Printf("namespace '%s'\n", namespace)
	logger.Printf("secret-name '%s'\n", secretName)
	logger.Println("secrets block:")
	logger.Println(secretsBlock)

	tmpl, err := template.New("template.yaml").ParseFiles(templateFileName)
	if err != nil {
		logger.Printf("template loading error: %s\n", err.Error())
		return "", err
	}

	data := struct {
		Namespace    string
		SecretName   string
		ServiceName  string
		SecretsBlock string
	}{
		Namespace:    namespace,
		SecretName:   secretName,
		ServiceName:  serviceName,
		SecretsBlock: secretsBlock,
	}

	var manifest bytes.Buffer

	err = tmpl.ExecuteTemplate(&manifest, "template.yaml", data)
	if err != nil {
		logger.Printf("manifest render error: %s\n", err.Error())
		return "", err
	}

	return manifest.String(), nil
}

func GetOrderedSecretValuesAsString(secrets map[string]string) string {
	keys := make([]string, 0)

	for key := range secrets {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	var sb strings.Builder

	for i := range keys {
		key := keys[i]

		sb.WriteString(fmt.Sprintf("  %s: %s", key, secrets[key]))
		if i != len(keys)-1 {
			sb.WriteString("\n")
		}
	}

	return sb.String()
}
