package k8s

import "testing"

func TestExtractServiceNameFromSecretName(t *testing.T) {
	result := ExtractServiceNameFromSecretName("cp.company.com-secret", []string{"secret"})

	if result != "cp.company.com" {
		t.Fatalf("unxpected result '%s', expected: 'cp.company.com'", result)
	}

	result = ExtractServiceNameFromSecretName("cp.company.com-redis-secret", []string{"redis-secret", "test", "whatever"})

	if result != "cp.company.com" {
		t.Fatalf("unxpected result '%s', expected: 'cp.company.com'", result)
	}

	result = ExtractServiceNameFromSecretName("cp.company.com--redis-secret", []string{"redis-secret", "test", "whatever"})

	if result != "cp.company.com" {
		t.Fatalf("unxpected result '%s', expected: 'cp.company.com'", result)
	}

	result = ExtractServiceNameFromSecretName("cp.company.com-secret", []string{})

	if result != "cp.company.com-secret" {
		t.Fatalf("unxpected result '%s', expected: 'cp.company.com-secret'", result)
	}

	result = ExtractServiceNameFromSecretName("webserver-secret-key", []string{})

	if result != "webserver-secret-key" {
		t.Fatalf("unxpected result '%s', expected: 'webserver-secret-key'", result)
	}
}
