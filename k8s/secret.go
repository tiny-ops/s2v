package k8s

import (
	"encoding/base64"
	"fmt"
	"gopkg.in/yaml.v3"
	"log"
	"os/exec"
	"strings"
)

const VaultPathValuePrefix = "vault:"

type KubernetesSecret struct {
	Data map[string]string
}

type SecretDecodingOptions struct {
	StopOnDecodeError bool
}

func GetSecretManifest(namespace string, secretName string, logger *log.Logger) (string, error) {
	logger.Printf("get secret manifest '%s' (namespace: '%s')\n", secretName, namespace)

	cmd := exec.Command("kubectl", "-n", namespace, "get", "secret", secretName, "-o", "yaml")

	out, err := cmd.CombinedOutput()

	if err != nil {
		fmt.Printf("kubectl error: %s\n", err.Error())
		return "", err
	}

	return string(out), nil
}

func GetSecretNamesFromNamespace(namespace string, nameMask string, logger *log.Logger) ([]string, error) {
	logger.Println("getting secret names from namespace", namespace)

	maskFilterEnabled := false

	if len(nameMask) > 0 {
		logger.Printf("filter by mask '%s'\n", nameMask)
		maskFilterEnabled = true
	}

	secretNames := make([]string, 0)

	cmd := exec.Command("kubectl", "-n", namespace, "get", "secrets", "--field-selector", "type=Opaque")

	out, err := cmd.CombinedOutput()

	if err != nil {
		fmt.Printf("kubectl error: %s\n", err.Error())
		return secretNames, err
	}

	stdout := string(out)

	outputLines := strings.Split(stdout, "\n")

	for i := range outputLines {
		if i != 0 {
			rowParts := strings.Split(outputLines[i], " ")

			if len(rowParts) > 1 {
				secretName := rowParts[0]

				if maskFilterEnabled {
					if strings.Contains(secretName, nameMask) {
						secretNames = append(secretNames, secretName)
						logger.Println("+ secret:", secretName)
					}

				} else {
					secretNames = append(secretNames, secretName)
					logger.Println("+ secret:", secretName)
				}
			}
		}
	}

	return secretNames, nil
}

func GetSecretsFromManifest(manifest *string, decodingOptions *SecretDecodingOptions, logger *log.Logger) (map[string]string, error) {
	results := make(map[string]string)

	logger.Println("loading secrets from manifest:")
	logger.Println("--------------------------------------")
	logger.Printf("%s\n", *manifest)
	logger.Println("--------------------------------------")

	var secret KubernetesSecret
	err := yaml.Unmarshal([]byte(*manifest), &secret)

	if err != nil {
		logger.Printf("unable to read secret: %s", err)
		return make(map[string]string), err
	}

	for key := range secret.Data {
		encryptedSecretValue, _ := secret.Data[key]

		secretValue := encryptedSecretValue

		decryptedValue, err := base64.StdEncoding.DecodeString(encryptedSecretValue)

		if err != nil {
			logger.Printf("unable to decode secret value: %s\n", err.Error())
			if decodingOptions.StopOnDecodeError {
				return make(map[string]string), err
			}
			logger.Println("using secret value as is (possible mistake in secret manifest)")

		} else {
			secretValue = string(decryptedValue)
		}

		results[key] = secretValue
	}

	logger.Printf("loaded secret: %+v\n", secret)

	return results, nil
}
