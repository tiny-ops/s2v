package k8s

import (
	"log"
	"os"
)

func GenerateSecretManifestFile(filePath string, content string, logger *log.Logger) error {
	f, err := os.Create(filePath)

	if err != nil {
		logger.Printf("unable to create file '%s': %s", filePath, err.Error())
		return err
	}

	defer f.Close()

	_, err = f.WriteString(content)

	if err != nil {
		logger.Printf("unable to write content into file '%s': %s", filePath, err.Error())
		return err
	}

	return nil
}
