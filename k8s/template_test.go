package k8s

import (
	"log"
	"os"
	"testing"
)

func TestGenerateSecretManifestFromTemplate(t *testing.T) {
	logger := log.New(os.Stderr, "", log.LstdFlags)

	var secrets = make(map[string]string)

	secrets["DATABASE_URL"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VUkw="
	secrets["DATABASE_USER"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VU0VS"
	secrets["DATABASE_PASSWORD"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9QQVNTV09SRA=="
	secrets["TOKEN"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNUT0tFTg=="

	secretsBlock := GetOrderedSecretValuesAsString(secrets)

	manifest, err := GenerateSecretManifestFromTemplate("../test-data/template.yaml", "demo", "app-secret", "app", secretsBlock, logger)

	if err != nil {
		t.Fatalf("unexpected error: %s", err.Error())
	}

	expectedManifestContent, err := os.ReadFile("../test-data/manifest.yaml")

	if err != nil {
		t.Fatalf("expected manifest read error: %s\n", err.Error())
	}

	if manifest != string(expectedManifestContent) {
		t.Fatalf("unexpected manifest result:\nexpected:\n-----\n%s\n-----\ngot:\n-----\n%s\n-----", string(expectedManifestContent), manifest)
	}

}

func TestGetOrderedSecretValuesAsString(t *testing.T) {
	var secrets = make(map[string]string)

	secrets["DATABASE_URL"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VUkw="
	secrets["DATABASE_USER"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VU0VS"
	secrets["DATABASE_PASSWORD"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9QQVNTV09SRA=="
	secrets["TOKEN"] = "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNUT0tFTg=="

	result := GetOrderedSecretValuesAsString(secrets)

	expectedResult := `  DATABASE_PASSWORD: dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9QQVNTV09SRA==
  DATABASE_URL: dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VUkw=
  DATABASE_USER: dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VU0VS
  TOKEN: dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNUT0tFTg==`

	if result != expectedResult {
		t.Fatalf("unexpected result:\nexpected:\n-----\n%s\n-----\ngot:\n-----\n%s\n-----", expectedResult, result)
	}

}
