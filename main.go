package main

import (
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"s2v/cmd"
)

func main() {
	file, err := os.OpenFile("s2v.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0755)

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	logger := log.New(file, "", log.LstdFlags)
	logger.Println("--------------------------------------------")
	logger.Printf("s2v v%s build %s\n", AppVersion, AppBuild)

	app := &cli.App{
		Commands: []*cli.Command{
			cmd.GetCopyCommand(logger),
			cmd.GetGenManifestsCommand(logger),
			cmd.GetAppendCommand(logger),
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
