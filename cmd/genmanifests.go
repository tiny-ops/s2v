package cmd

import (
	"errors"
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"s2v/k8s"
	"s2v/vault"
	"strings"
)

func GetGenManifestsCommand(logger *log.Logger) *cli.Command {
	return &cli.Command{
		Name:    "gen-manifests",
		Aliases: []string{"gm"},
		Usage:   "generate secret manifests with vault paths",
		Flags: []cli.Flag{
			GetK8sNameSpaceFlag(),
			&cli.StringFlag{
				Name:        "secret-suffixes",
				Usage:       "Suffixes for secrets to extract original service name, i.e. secret,redis-secret (use comma as separator)",
				DefaultText: "secret",
				Value:       "secret",
				Required:    false,
			},
			GetValueBasePathFlag(),
			&cli.StringFlag{
				Name:        "output-dir",
				Usage:       "output directory name, i.e. 'manifests'",
				DefaultText: "manifests",
				Value:       "manifests",
				Required:    false,
			},
			&cli.BoolFlag{
				Name:        "sort-keys",
				Usage:       "sort secret key names (A-Z))",
				DefaultText: "true",
				Value:       true,
			},
			GetIgnoreBase64ErrorsFlag(),
			GetFilterSecretByMaskFlag(),
			GetVaultDestPathFlag(),
			&cli.StringFlag{
				Name:        "dest-service-name",
				Usage:       "destination service name",
				DefaultText: "",
				Value:       "",
				Required:    false,
			},
		},
		Action: func(cCtx *cli.Context) error {
			namespace := cCtx.String("k8s-namespace")
			vaultBasePath := cCtx.String("vault-base-path")
			vaultDestPath := cCtx.String("vault-dest-path")
			destServiceName := cCtx.String("dest-service-name")
			secretSuffixesArg := cCtx.String("secret-suffixes")
			secretSuffixes := strings.Split(secretSuffixesArg, ",")
			outputDir := cCtx.String("output-dir")
			sortKeys := cCtx.Bool("sort-keys")
			ignoreBase64Errors := cCtx.Bool("ignore-base64-errors")
			secretNameMask := cCtx.String("secret-mask")

			logger.Println("generate K8s secret manifests with paths to hashicorp vault..")
			logger.Println("k8s-namespace:", namespace)
			logger.Println("vault-base-path:", vaultBasePath)
			logger.Println("vault-dest-path:", vaultDestPath)
			logger.Println("dest-service-name:", destServiceName)
			logger.Println("secret-suffixes:", secretSuffixes)
			logger.Println("output-dir:", outputDir)
			logger.Println("sort-keys:", sortKeys)
			logger.Println("ignore-base64-errors:", ignoreBase64Errors)
			logger.Println("secret-mask:", secretNameMask)

			CheckEnvVars()

			secretNames, err := k8s.GetSecretNamesFromNamespace(namespace, secretNameMask, logger)

			if err != nil {
				logger.Println("unable to get k8s secret names:", err.Error())
				os.Exit(1)
			}

			logger.Println("secret names:", secretNames)

			decodingOpts := k8s.SecretDecodingOptions{
				StopOnDecodeError: !ignoreBase64Errors,
			}

			if _, err := os.Stat(outputDir); errors.Is(err, os.ErrNotExist) {
				err := os.Mkdir(outputDir, os.ModePerm)
				if err != nil {
					log.Println(err)
				}
			}

			for i := range secretNames {
				secretName := secretNames[i]
				logger.Println("processing secret", secretName)

				serviceName := destServiceName

				if len(destServiceName) == 0 {
					serviceName = k8s.ExtractServiceNameFromSecretName(secretName, secretSuffixes)
				}

				logger.Println("service name:", serviceName)

				manifest, err := k8s.GetSecretManifest(namespace, secretName, logger)

				if err != nil {
					logger.Printf("unable to get secret manifest '%s': %s", secretName, err.Error())
					os.Exit(1)
				}

				secrets, err := k8s.GetSecretsFromManifest(&manifest, &decodingOpts, logger)

				if err != nil {
					logger.Printf("unable to read secrets from '%s': %s", secretName, err.Error())
					os.Exit(1)
				}

				updatedSecrets := vault.GetSecretsWithVaultPaths(vaultBasePath, serviceName, secrets, vaultDestPath)

				orderedSecretsBlock := k8s.GetOrderedSecretValuesAsString(updatedSecrets)

				manifest, err = k8s.GenerateSecretManifestFromTemplate("template.yaml", namespace, secretName, serviceName, orderedSecretsBlock, logger)

				if err != nil {
					logger.Printf("unable to generate secret manifest from template: %s", err.Error())
					os.Exit(1)
				}

				manifestFilePath := fmt.Sprintf("%s/%s.yaml", outputDir, secretName)

				err = k8s.GenerateSecretManifestFile(manifestFilePath, manifest, logger)

				if err != nil {
					logger.Printf("unable to create manifest file '%s': %s\n", manifestFilePath, err.Error())
					os.Exit(1)
				}
			}

			return nil
		},
	}
}
