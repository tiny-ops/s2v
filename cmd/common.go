package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"os"
)

func CheckEnvVars() {
	vars := [1]string{"KUBECONFIG"}

	for i := range vars {
		varName := vars[i]

		_, present := os.LookupEnv(varName)

		if !present {
			fmt.Printf("error: environment variables must be set: %s\n", vars)
			os.Exit(1)
		}
	}

	CheckVaultEnvVars()
}

func CheckVaultEnvVars() {
	vars := [2]string{"VAULT_ADDR", "VAULT_TOKEN"}

	for i := range vars {
		varName := vars[i]

		_, present := os.LookupEnv(varName)

		if !present {
			fmt.Printf("error: environment variables must be set: %s\n", vars)
			os.Exit(1)
		}
	}
}

func GetValueBasePathFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:     "vault-base-path",
		Usage:    "base vault path, i.e. 'kv/demo'",
		Required: true,
	}
}

func GetK8sNameSpaceFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:        "k8s-namespace",
		Usage:       "kubernetes namespace",
		DefaultText: "default",
		Value:       "default",
		Required:    false,
	}
}

func GetIgnoreBase64ErrorsFlag() *cli.BoolFlag {
	return &cli.BoolFlag{
		Name:  "ignore-base64-errors",
		Usage: "ignore base64 decode errors, use secret value as is",
	}
}

func GetContinueFromSecretFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:        "continue-from-secret",
		Usage:       "continue command from secret with name. Use secret name from kubernetes",
		DefaultText: "",
		Value:       "",
		Required:    false,
	}
}

func GetFilterSecretByMaskFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:        "secret-mask",
		Usage:       "process secrets contains specified mask in name",
		DefaultText: "",
		Value:       "",
		Required:    false,
	}
}

func GetVaultDestPathFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:        "vault-dest-path",
		Usage:       "destination path inside vault (without ../data/.. in path)",
		DefaultText: "",
		Value:       "",
		Required:    false,
	}
}
