package cmd

import (
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"s2v/vault"
)

func GetAppendCommand(logger *log.Logger) *cli.Command {
	return &cli.Command{
		Name:    "append",
		Aliases: []string{"a"},
		Usage:   "append vault secrets from source path to destination path",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "vault-src-path",
				Usage:       "vault source path",
				DefaultText: "kv/data/namespace/service",
				Required:    true,
			},
			&cli.StringFlag{
				Name:        "vault-dest-path",
				Usage:       "vault destination path",
				DefaultText: "kv/namespace/service (don't use '../data/..' in path)",
				Required:    true,
			},
		},
		Action: func(cCtx *cli.Context) error {
			vaultSrcPath := cCtx.String("vault-src-path")
			vaultDestPath := cCtx.String("vault-dest-path")

			logger.Println("appending vault secrets from source path to destination path..")
			logger.Println("vault-src-path:", vaultSrcPath)
			logger.Println("vault-dest-path:", vaultDestPath)

			CheckVaultEnvVars()

			err := vault.AppendSecretsInVault(vaultSrcPath, vaultDestPath, logger)

			if err != nil {
				logger.Println("unable to append vault secrets from path to path:", err.Error())
				os.Exit(1)
			}

			return nil
		},
	}
}
