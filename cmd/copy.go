package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"s2v/k8s"
	"s2v/vault"
	"strings"
)

func GetCopyCommand(logger *log.Logger) *cli.Command {
	return &cli.Command{
		Name:    "copy",
		Aliases: []string{"c"},
		Usage:   "copy k8s vanilla secrets to HashiCorp Vault",
		Flags: []cli.Flag{
			GetK8sNameSpaceFlag(),
			GetIgnoreBase64ErrorsFlag(),
			&cli.StringFlag{
				Name:        "secret-suffixes",
				Usage:       "Suffixes for secrets to extract original service name, i.e. secret,redis-secret (use comma as separator)",
				DefaultText: "secret",
				Value:       "secret",
				Required:    false,
			},
			GetValueBasePathFlag(),
			GetContinueFromSecretFlag(),
			GetFilterSecretByMaskFlag(),
			GetVaultDestPathFlag(),
		},
		Action: func(cCtx *cli.Context) error {
			namespace := cCtx.String("k8s-namespace")
			vaultBasePath := cCtx.String("vault-base-path")
			vaultDestPath := cCtx.String("vault-dest-path")
			secretSuffixesArg := cCtx.String("secret-suffixes")
			secretSuffixes := strings.Split(secretSuffixesArg, ",")
			ignoreBase64Errors := cCtx.Bool("ignore-base64-errors")
			continueFromSecretName := cCtx.String("continue-from-secret")
			secretNameMask := cCtx.String("secret-mask")

			logger.Println("copying K8s secrets to hashicorp Vault..")
			logger.Println("k8s-namespace:", namespace)
			logger.Println("vault-base-path:", vaultBasePath)
			logger.Println("vault-dest-path:", vaultDestPath)
			logger.Println("secret-suffixes:", secretSuffixes)
			logger.Println("ignore-base64-errors:", ignoreBase64Errors)
			logger.Println("continue-from-secret:", continueFromSecretName)
			logger.Println("secret-mask:", secretNameMask)

			CheckEnvVars()

			secretNames, err := k8s.GetSecretNamesFromNamespace(namespace, secretNameMask, logger)

			if err != nil {
				logger.Println("unable to get k8s secret names:", err.Error())
				os.Exit(1)
			}

			logger.Println("secret names:", secretNames)

			decodingOpts := k8s.SecretDecodingOptions{
				StopOnDecodeError: !ignoreBase64Errors,
			}

			continueSecretFound := len(continueFromSecretName) == 0

			for i := range secretNames {
				secretName := secretNames[i]

				if !continueSecretFound {
					if secretName == continueFromSecretName {
						logger.Printf("continue process from secret '%s'\n", secretName)
						continueSecretFound = true

					} else {
						continue
					}
				}

				logger.Println("processing secret", secretName)

				serviceName := k8s.ExtractServiceNameFromSecretName(secretName, secretSuffixes)
				logger.Println("service name:", serviceName)

				manifest, err := k8s.GetSecretManifest(namespace, secretName, logger)

				if err != nil {
					logger.Printf("unable to get secret manifest '%s': %s", secretName, err.Error())
					os.Exit(1)
				}

				secrets, err := k8s.GetSecretsFromManifest(&manifest, &decodingOpts, logger)

				if err != nil {
					logger.Printf("unable to read secrets from '%s': %s", secretName, err.Error())
					os.Exit(1)
				}

				if len(secrets) > 0 {
					skipSecretTransfer := false

					for key := range secrets {
						value := secrets[key]

						if strings.HasPrefix(value, k8s.VaultPathValuePrefix) {
							skipSecretTransfer = true
							break
						}
					}

					if skipSecretTransfer {
						logger.Printf("secret '%s' already has vault paths, skip\n", secretName)
						continue
					}

					vaultServiceSecretPath := fmt.Sprintf("%s/%s", vaultBasePath, serviceName)

					if len(vaultDestPath) > 0 && len(secretNameMask) > 0 {
						logger.Printf("vault destination path will be overrided with '%s'\n", vaultDestPath)
						vaultServiceSecretPath = vaultDestPath
					}

					logger.Println("vault path for service:", vaultServiceSecretPath)

					err = vault.CreateSecretsInVault(vaultServiceSecretPath, secrets, logger)

					if err != nil {
						logger.Printf("unable to create secrets in vault at path '%s': %s", vaultServiceSecretPath, err.Error())
						os.Exit(1)
					}

					logger.Printf("secret '%s' values copied into vault\n", secretName)
				}
			}

			return nil
		},
	}
}
