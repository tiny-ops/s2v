package vault

import (
	"fmt"
	"log"
	"s2v/util"
	"strings"
)

func AppendSecretsInVault(vaultSrcPath string, vaultDestPath string, logger *log.Logger) error {
	logger.Printf("appending secrets from source vault path '%s' to '%s'..", vaultSrcPath, vaultDestPath)

	if !strings.Contains(vaultSrcPath, "/data/") {
		logger.Fatal("error: source vault path doesn't contain /data/ in path")
	}

	if strings.Contains(vaultDestPath, "/data/") {
		logger.Fatal("error: destination vault path contains /data/ in path")
	}

	vaultDestReadPath := addDataToPath(vaultDestPath)

	logger.Println("reading secrets from destination path", vaultDestReadPath)

	secrets := make(map[string]string)

	secrets, err := readSecretsFromVaultPath(vaultDestReadPath, logger)

	if err != nil {
		fmt.Printf("unable to read secrets from destination vault path, possible path doesn't exist (continue): %s", err.Error())
	}

	logger.Println("reading secrets from source path", vaultSrcPath)

	secretsToAppend, err := readSecretsFromVaultPath(vaultSrcPath, logger)

	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	logger.Println("secrets to append:")
	for key := range secretsToAppend {
		logger.Printf("- '%s'\n", key)
		secrets[key] = secretsToAppend[key]
	}

	logger.Println("appending secrets to a destination path", vaultDestPath)

	err = CreateSecretsInVault(vaultDestPath, secrets, logger)

	if err != nil {
		fmt.Println("unable to append secrets", err.Error())
		return err
	}

	logger.Println("secrets were successfully appended")

	return nil
}

func readSecretsFromVaultPath(vaultPath string, logger *log.Logger) (map[string]string, error) {
	stdout, err := util.ExecuteShellCommand("vault", "read", vaultPath, "-format=json")

	if err != nil {
		fmt.Println(err.Error())
		return make(map[string]string), err
	}

	return GetSecretsFromReadJson(stdout, logger)
}
