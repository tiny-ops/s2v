package vault

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

func CreateSecretsInVault(vaultPath string, secrets map[string]string, logger *log.Logger) error {
	logger.Println("putting secrets to vault path:", vaultPath)

	var argsBuilder strings.Builder

	argsBuilder.WriteString(fmt.Sprintf("kv put %s ", vaultPath))

	multiLineValueFiles := make([]string, 0)

	for key := range secrets {
		logger.Printf("- secret '%s'\n", key)
		secretValue := secrets[key]

		if !strings.HasPrefix(secretValue, "vault:") {
			if strings.Contains(secretValue, "\n") || strings.Contains(secretValue, "\"") ||
				strings.Contains(secretValue, ",") || strings.Contains(secretValue, " ") {
				fileName, err := writeComplexValueIntoFile(key, secretValue, logger)

				if err != nil {
					logger.Printf("unable to write secret complex value into file: %s", err.Error())
					return err
				}

				multiLineValueFiles = append(multiLineValueFiles, fileName)

				argsBuilder.WriteString(fmt.Sprintf("%s=@%s ", key, fileName))

			} else {
				argsBuilder.WriteString(fmt.Sprintf("%s=%s ", key, secretValue))
			}
		}
	}

	secretArgs := strings.Split(argsBuilder.String(), " ")

	logger.Printf("args: %s\n", secretArgs)

	cmd := exec.Command("vault", secretArgs...)

	out, err := cmd.CombinedOutput()

	if err != nil {
		logger.Printf("vault cli error: %s\n", err.Error())
		return err
	}

	output := string(out)

	logger.Println("vault output:")
	logger.Println(output)

	for i := range multiLineValueFiles {
		filePath := multiLineValueFiles[i]
		err := os.Remove(filePath)

		if err != nil {
			logger.Printf("unable to remove temporary file '%s': %s", filePath, err.Error())
		}
	}

	return nil
}

func writeComplexValueIntoFile(keyName string, value string, logger *log.Logger) (string, error) {
	if _, err := os.Stat("tmp"); os.IsNotExist(err) {
		err := os.Mkdir("tmp", 0750)

		if err != nil {
			logger.Printf("unable to create directory 'tmp': %s", err.Error())
			return "", err
		}
	}

	fileName := fmt.Sprintf("tmp/%s.tmp", keyName)

	f, err := os.Create(fileName)

	if err != nil {
		logger.Printf("unable to create file '%s': %s", fileName, err.Error())
		return "", err
	}

	defer f.Close()

	_, err = f.WriteString(value)

	if err != nil {
		logger.Printf("unable to write content into file '%s': %s", fileName, err.Error())
		return "", err
	}

	return fileName, nil
}
