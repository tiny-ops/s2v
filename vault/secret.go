package vault

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"strings"
)

func GetSecretsWithVaultPaths(vaultBasePath string, serviceName string, secrets map[string]string, vaultDestPath string) map[string]string {
	results := make(map[string]string)

	for key := range secrets {
		value := GetVaultSecretReadPath(vaultBasePath, serviceName, key, vaultDestPath)
		encodedValue := b64.StdEncoding.EncodeToString([]byte(value))
		results[key] = encodedValue
	}

	return results
}

func GetVaultSecretReadPath(basePath string, serviceName string, secretKeyName string, vaultDestPath string) string {
	if len(vaultDestPath) > 0 {
		return fmt.Sprintf("vault:%s#%s", insertDataAfterFirstPart(vaultDestPath), secretKeyName)

	} else {
		return fmt.Sprintf("vault:%s/%s#%s", basePath, serviceName, secretKeyName)
	}
}

// insertDataAfterFirstPart takes a string input with parts separated by "/"
// and inserts "/data/" after the first part of the input.
func insertDataAfterFirstPart(input string) string {
	parts := strings.SplitN(input, "/", 2)

	// Check if there are at least two parts to modify the string accordingly.
	if len(parts) > 1 {
		return parts[0] + "/data/" + parts[1]
	}

	return input
}

// GetSecretsFromReadJson
//
// Extract secrets from vault read command output
func GetSecretsFromReadJson(vaultReadOutput string, logger *log.Logger) (map[string]string, error) {
	type VaultReadOutput struct {
		Data struct {
			Data map[string]string `json:"data"`
		} `json:"data"`
	}

	var vaultOutput VaultReadOutput
	err := json.Unmarshal([]byte(vaultReadOutput), &vaultOutput)
	if err != nil {
		logger.Printf("vault output parse error: %s", err.Error())
		return nil, err
	}

	return vaultOutput.Data.Data, nil
}

// addDataToPath
//
// Add /data/ in vault path
func addDataToPath(input string) string {
	parts := strings.SplitN(input, "/", 2)
	if len(parts) < 2 {
		log.Println("Invalid input format")
		return input
	}
	return parts[0] + "/data/" + parts[1]
}
