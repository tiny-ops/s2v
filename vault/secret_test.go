package vault

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"testing"
)

func TestUpdateValuesToVaultPaths(t *testing.T) {
	var secrets = make(map[string]string)

	secrets["DATABASE_URL"] = "c29tZXRoaW5n"
	secrets["TOKEN"] = "c29tZXRoaW5n"

	results := GetSecretsWithVaultPaths("kv/data/demo", "app", secrets, "")

	if results["DATABASE_URL"] != "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VUkw=" {
		t.Fatalf("unexpected value for 'DATABASE_URL': expected 'dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNEQVRBQkFTRV9VUkw=', got '%s'", results["DATABASE_URL"])
	}

	if results["TOKEN"] != "dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNUT0tFTg==" {
		t.Fatalf("unexpected value for 'DATABASE_URL': expected 'dmF1bHQ6a3YvZGF0YS9kZW1vL2FwcCNUT0tFTg==', got '%s'", results["TOKEN"])
	}
}

func TestGetSecretsFromReadJson(t *testing.T) {
	logger := log.New(os.Stdout, "LOG: ", log.Ldate|log.Ltime|log.Lshortfile)

	tests := []struct {
		name    string
		input   string
		want    map[string]string
		wantErr bool
	}{
		{
			name: "valid input",
			input: `{
				"data": {
					"data": {
						"SOMEKEY1": "SOMEVALUE1",
						"SOMEKEY2": "SOMEVALUE2"
					}
				}
			}`,
			want:    map[string]string{"SOMEKEY1": "SOMEVALUE1", "SOMEKEY2": "SOMEVALUE2"},
			wantErr: false,
		},
		{
			name: "invalid json format",
			input: `{
				"data": "invalid"
			}`,
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty data",
			input: `{
				"data": {
					"data": {}
				}
			}`,
			want:    map[string]string{},
			wantErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got, err := GetSecretsFromReadJson(tc.input, logger)
			if (err != nil) != tc.wantErr {
				t.Errorf("GetSecretsFromReadJson() error = %v, wantErr %v", err, tc.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tc.want) {
				t.Errorf("GetSecretsFromReadJson() = %v, want %v", got, tc.want)
			}
		})
	}
}

func TestAddDataToPath(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  string
	}{
		{
			name:  "kv namespace service",
			input: "kv/namespace/service",
			want:  "kv/data/namespace/service",
		},
		{
			name:  "cubby demo somethingelse",
			input: "cubby/demo/somethingelse",
			want:  "cubby/data/demo/somethingelse",
		},
		{
			name:  "cubby vip somethingelse",
			input: "cubby/vip/somethingelse",
			want:  "cubby/data/vip/somethingelse",
		},
		{
			name:  "single block",
			input: "kv",
			want:  "kv",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := addDataToPath(tt.input); got != tt.want {
				t.Errorf("addDataToPath() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetVaultSecretReadPath(t *testing.T) {
	tests := []struct {
		basePath      string
		serviceName   string
		secretKeyName string
		vaultDestPath string
		expected      string
	}{
		{"base", "service", "key", "", "vault:base/service#key"},
		{"base", "service", "key", "kv/demo/dest", "vault:kv/data/demo/dest#key"},
		// Add more test cases as needed, including edge cases
	}

	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s/%s/%s/%s", tt.basePath, tt.serviceName, tt.secretKeyName, tt.vaultDestPath), func(t *testing.T) {
			result := GetVaultSecretReadPath(tt.basePath, tt.serviceName, tt.secretKeyName, tt.vaultDestPath)
			if result != tt.expected {
				t.Errorf("GetVaultSecretReadPath(%s, %s, %s, %s) got %s, want %s", tt.basePath, tt.serviceName, tt.secretKeyName, tt.vaultDestPath, result, tt.expected)
			}
		})
	}
}
